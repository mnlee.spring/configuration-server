<a href="https://mnlee.gitlab.io/portfolio/#gitlab">Back to Margaret Lee's Portfolio</a>

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Execution](#execution)
* [Testing](#testing)
* [Output](#output)


## General info
* Establish connections from Spring Cloud Config Server to local Git repository
	
## Technologies
Project is created with:
* Spring Boot version: 2.1.2
* Java version: 1.8
* Maven 3.3.9
* Git
	
## Setup
* Create project using https://start.spring.io/ or Spring Boot CLI with dependencies
-- devtools, config server

### Steps
* Add contents to
	- application.properties
	- ConfigServerApplication.java
* Setup Git Repository (/home/mnlee/WorkSpaces/STS_MicroServices/git-localconfig-repo)
	- Add link to Git repository (Build Path -> Link Source)

* Add contents to recipe-config-service.properties and commit to git

* Copy recipe-config-service.properties to recipe-config-service-dev.properties and recipe-config-service-qa.properties, notice the naming convention.

* Add docker see -- https://www.callicoder.com/spring-boot-docker-example/
	- dockerhub -- https://hub.docker.com/

## Execution
* http://localhost:8888/recipe-config-service/default
* http://localhost:8888/recipe-config-service/qa
* http://localhost:8888/recipe-config-service/dev
	
## Testing
* browser

	
## Output

{
"name": "file:///home/mnlee/WorkSpaces/STS_MicroServices/git-localconfig-repo/recipe-config-service-dev.properties",
"source": {
"recipe-config-service.admin-user": "username_dev",
"recipe-config-service.admin-password": "password_dev"
}
},
..
"source": {
"recipe-config-service.admin-user": "username",
"recipe-config-service.admin-password": "password"
}
..






 